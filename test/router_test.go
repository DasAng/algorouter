package test

import (
	"testing"
	"fmt"
	"gitlab.com/DasAng/algorouter"
	"net/http"
	"net/http/httptest"
	"strings"
	"io/ioutil"
)

var service algorouter.Microservice
var server *httptest.Server

func init() {

	service = algorouter.Microservice{}
	service.Initialize()

	service.Get("/ping", Ping,MyMiddleware)
	service.Post("/create", Create,MyMiddleware)

	server = httptest.NewServer(service.Router)
}

func TestHttpGetRoute(t *testing.T) {

	t.Log("Test started")

	urlStr := fmt.Sprintf("%s/ping",server.URL)

	request, err := http.NewRequest("GET",urlStr,nil)
	if err != nil {
		t.Fatalf(err.Error())
		return
	}

	res, err := http.DefaultClient.Do(request)
	if err != nil {
		t.Fatalf(err.Error())
		return
	}

	if res.StatusCode != 200 {
		t.Errorf("Success expected: %d", res.StatusCode) //Uh-oh this means our test failed
		return
	}

	t.Log("Test success")
}

func TestHttpPostRoute(t *testing.T) {

	t.Log("Test started")

	urlStr := fmt.Sprintf("%s/create",server.URL)

	requestJson := `{"username": "dennis", "balance": 200}`

	reader := strings.NewReader(requestJson)

	request, err := http.NewRequest("POST",urlStr,reader)
	if err != nil {
		t.Fatalf(err.Error())
		return
	}

	res, err := http.DefaultClient.Do(request)
	if err != nil {
		t.Fatalf(err.Error())
		return
	}

	if res.StatusCode != 200 {
		t.Errorf("Success expected: %d", res.StatusCode) //Uh-oh this means our test failed
		return
	}

	t.Log("Test success")
}

func MyMiddleware(rw http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	fmt.Println("My middleware start")
	next(rw, r)
}

func Ping(res http.ResponseWriter, req *http.Request) {
	fmt.Println("Ping")
	res.WriteHeader(200)
}

func Create(res http.ResponseWriter, req *http.Request) {
	fmt.Println("Create")

	_, err := ioutil.ReadAll(req.Body)
	if err != nil {
		fmt.Printf("Error: %s\n", err)
		res.WriteHeader(http.StatusInternalServerError)
		return
	}

	res.WriteHeader(200)
}